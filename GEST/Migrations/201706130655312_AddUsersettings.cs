namespace GEST.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUsersettings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CurrentLogin = c.String(maxLength: 4000),
                        CurrentPassword = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserSettings");
        }
    }
}
