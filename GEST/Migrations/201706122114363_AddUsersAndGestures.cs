namespace GEST.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUsersAndGestures : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Gestures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GestureName = c.String(maxLength: 255),
                        Action = c.String(maxLength: 255),
                        Quantity = c.Int(nullable: false),
                        GestUser_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GestUsers", t => t.GestUser_Id)
                .Index(t => t.GestUser_Id);
            
            CreateTable(
                "dbo.GestUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 70),
                        Name = c.String(maxLength: 70),
                        Password = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Gestures", "GestUser_Id", "dbo.GestUsers");
            DropIndex("dbo.Gestures", new[] { "GestUser_Id" });
            DropTable("dbo.GestUsers");
            DropTable("dbo.Gestures");
        }
    }
}
