namespace GEST.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdminUser : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT GestUsers ON");
            Sql("INSERT INTO  GestUsers (Id, Login, Name, Password) VALUES (1, 'admin', 'Admin', 'admin')");
            Sql("ALTER TABLE GestUsers ALTER COLUMN Id IDENTITY(2, 1)");

            Sql("SET IDENTITY_INSERT GestUsers OFF");
        }
        
        public override void Down()
        {
        }
    }
}
