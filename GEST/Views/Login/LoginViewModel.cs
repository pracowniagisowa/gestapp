﻿using GEST.ViewModels;
using System.Windows.Input;
using KinectReader;
using System.Windows;
using System.Linq;
using System.Windows.Controls;

namespace GEST.Views
{
    class LoginViewModel : BaseViewModel
    {
        #region Private Properties

        private string _fillFields { get; set; } = "Wypełnij login";       
        private string _loginIncorrect { get; set; } = "Jestś nowym użytkownikiem? Zarejestruj się";
        private string _passwordIncorrect { get; set; } = "Błędne hasło!";

        #endregion

        #region Public Properties

        public string Login { get; set; }
        public string Password { get; set; }
        public string TxtPassword { get; set; }



        //public ICommand LoginCommand { get; set; }
        public ICommand NewLoginCommand { get; set; }

        public ICommand NewUserPageCommand { get; set; }

        #endregion

        #region Constructor

        public LoginViewModel()
        {
               
            //LoginCommand = new RelayCommand(LoginAction);
            NewLoginCommand = new RelayCommandWithP<object>(NewLoginAction);

            NewUserPageCommand = new RelayCommand(AddNewUserPage);

            var db = new GestDbContext();
            db.GestUsers.Any();
                        
        }

        #endregion

        #region Helper Methods

        //private void LoginAction()
        //{
        //    if (Login != null)
        //    {
        //        using (var db = new GestDbContext())
        //        {
        //            GestUser currentUser = db.GestUsers.FirstOrDefault(u => u.Login == Login);
        //            if (currentUser != null)
        //            {
        //                if (Password == currentUser.Password)
        //                {
        //                    ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentUser = currentUser;
        //                    ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentPage = AppPage.Record;
        //                }
        //            }
        //            else
        //            {
        //                MessageBox.Show(_loginIncorrect);
        //            }
        //        }                
        //    }
        //    else
        //    {
        //        MessageBox.Show(_fillFields);
        //    }
        //}

        private void NewLoginAction(object parameter)
        {
            var passwordBox = parameter as PasswordBox;
            TxtPassword = passwordBox.Password;
            Password = TxtPassword;


            if (Login != null)
            {
                using (var db = new GestDbContext())
                {
                    GestUser currentUser = db.GestUsers.FirstOrDefault(u => u.Login == Login);
                    if (currentUser != null)
                    {
                        if (Password == currentUser.Password)
                        {
                            ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentUser = currentUser;
                            ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentPage = AppPage.Record;
                        }
                    }
                    else
                    {
                        MessageBox.Show(_loginIncorrect);
                    }
                }
            }
            else
            {
                MessageBox.Show(_fillFields);
            }
        }


        private void AddNewUserPage()
        {
            ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentPage = AppPage.NewUser;
        }



        #endregion
    }
}
