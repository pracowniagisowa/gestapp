﻿using System.Windows.Controls;

namespace GEST.Views.AddGesture
{
    /// <summary>
    /// Interaction logic for AddGesture.xaml
    /// </summary>
    public partial class AddGesture : Page
    {
        public AddGesture()
        {
            InitializeComponent();
            DataContext = new AddGestureViewModel();
        }
    }
}
