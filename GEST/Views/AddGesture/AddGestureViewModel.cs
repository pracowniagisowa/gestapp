﻿using GEST.DataModels;
using GEST.Helpers;
using GEST.MOOCs;
using GEST.Services;
using GEST.ViewModels;
using GEST.Views.Admin;
using KinectReader;
using KinectReader.Models.Helpers;
using Microsoft.Kinect;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Input;

namespace GEST.Views.AddGesture
{
    class AddGestureViewModel : BaseViewModel, IObserver
    {
        #region Private Properties
        
        private const string START_RECORDING = "Rozpocznij nagrywanie";
        private const string STOP_RECORDING = "Zakończ nagrywanie";
        private const string MATLAB_ADD_GESTURE = "addGesture";
        private const string MATLAB_UPDATE_MODEL = "updateModels";
        private const string ADD_GESTURE_CONFIRMATION = "Czy wysłać gest do bazy danych?";
        private const string REPEAT_GESTURE = "Gest do powtórzenia";

        private string _currentLogin;
        private IDataRecorderService _dataRecorder;
        private Body _body;
        private Gesture _selectedGesture;
        private DrawScaletonService _drawScaletonService;
        private IMessageService _messageService;
        private RequestsService _requestsService;
        
        private int _canvasInitHeight { get; set; } = 268;
        private int _canvasInitWidth { get; set; } = 580;

        Timer timer = new Timer();

        #endregion

        #region Public Properties
        public bool Recording { get; set; }
        public string RecordButtonContent { get; set; }
        public bool RecordButtonEnabled { get; set; }
        public bool UpdateModelButtonEnabled { get; set; } = true;
        public bool MatlabProgressBar { get; set; } = false;
        public int Counter { get; set; }
        public string CounterVisibility { get; set; } = "Hidden";
        public string Message { get; set; }
        public ObservableCollection<Line> LinesObservable { get; set; }
        public int GesturesToUpdate { get; set; } = 0;
        public string UpdateButtonContent { get; set; } = "Aktualizuj";

        public ICommand AddGestureCommand { get; set; }
        public ICommand DiscardCommand { get; set; }
        public ICommand SetCanvasSizeCommand { get; set; }
        public ICommand UpdateModelCommand { get; set; }

        #endregion

        #region Constructor

        public AddGestureViewModel()
        {
            KinectHelper.Instance().addObserver(this);
            _messageService = new WindowsMessageService();
            _dataRecorder = new DataRecorderService(new MatFileSaverService());
            _currentLogin = ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentUser.Login;
            _drawScaletonService = new DrawScaletonService(_canvasInitHeight, _canvasInitWidth);
            _requestsService = new RequestsService();

            LinesObservable = new ObservableCollection<Line>();
            
            AddGestureCommand = new RelayCommand(AddGesture);
            DiscardCommand = new RelayCommand(Discard);
            SetCanvasSizeCommand = new RelayCommand(SetCanvasSize);
            UpdateModelCommand = new RelayCommand(UpdateModel);

            RecordButtonContent = START_RECORDING;
            RecordButtonEnabled = true;
            CounterVisibility = "Hidden";       
        }

        #endregion             

        #region Helper Methods
        private async void AddGesture()
        {
            if (!Recording)
            {
                RecordButtonContent = STOP_RECORDING;
                UnBlockUI();// = true;
                Recording = true;
            }
            else
            {
                // zatrzymywanie nagrywania
                Recording = false;
                
                string matFilePath = _dataRecorder.StopRecording();
                bool addGestureDecision = _messageService.ShowAndAsk(ADD_GESTURE_CONFIRMATION);
                if (addGestureDecision)
                {
                    BlockUI();
                    MatlabProgressBar = true;

                    ResponseData responseData = await _requestsService.AddGesture(
                        _currentLogin, 
                        matFilePath, 
                        ShareObject.Instance().CurrentGesture.GestureName);

                    if (responseData.GestureOk)
                    {
                        using (var db = new GestDbContext())
                        {
                            Gesture recognizedGesture = db.Gestures.FirstOrDefault(g => g.GestureName == responseData.GestureName);
                            recognizedGesture.Quantity = responseData.GestureCount;
                            db.SaveChanges();
                            GesturesToUpdate += 1;
                            UpdateButtonContent = String.Format("Aktualizuj ({0})", GesturesToUpdate);
                            UpdateModelButtonEnabled = true;
                            MatlabProgressBar = false;
                        }
                    }
                    else
                    {
                        _messageService.Show(REPEAT_GESTURE);
                    }
                } 
                RecordButtonContent = START_RECORDING;
            }
        }


        private void Discard()
        {
            ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentPage = AppPage.Admin;
        }

        private async void UpdateModel()
        {
            MatlabProgressBar = true;
            BlockUI();

            ResponseData responseData = await _requestsService.UpdateModels(_currentLogin);

            MatlabProgressBar = false;
            UnBlockUI();

            if (responseData.GestureOk)
            {
                GesturesToUpdate = 0;
                UpdateButtonContent = String.Format("Aktualizuj ({0})", GesturesToUpdate);
                _messageService.Show("Zaktualizowano");
            }
            else
            {
                _messageService.Show("nie zaktualizowano.....");
            }
        }

        public void updateData()
        {
            Message = KinectHelper.Message;
            _body = KinectHelper.Body;

            LinesObservable = _drawScaletonService.DrawScaleton(_body);

            if (Recording)
            {
                _dataRecorder.StartRecording(_body);
            }
        }

        private void SetCanvasSize()
        {

        }

        private void BlockUI()
        {
            UpdateModelButtonEnabled = false;
            RecordButtonEnabled = false;
        }

        private void UnBlockUI()
        {
            UpdateModelButtonEnabled = true;
            RecordButtonEnabled = true;
        }

        #endregion
    }
}



//

            

            