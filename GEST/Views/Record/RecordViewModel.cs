﻿using GEST.DataModels;
using GEST.Helpers;
using GEST.Services;
using GEST.Views;
using KinectReader;
using KinectReader.Models.Helpers;
using Microsoft.Kinect;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GEST.ViewModels
{
    class RecordViewModel : BaseViewModel, IObserver
    {
        #region Private Propserties   

        private const string NO_BODY = "Osoba znajduje się poza zasięgiem sensora kinekt";

        private int _canvasInitHeight { get; set; } = 268;
        private int _canvasInitWidth { get; set; } = 580;

        private bool _recordBody;// = true;
        private string _currentLogin { get; set; }
       
        private IDataRecorderService _dataRecorder;
        private DrawScaletonService _drawScaletonService;
        private IMatFileSaverService _matFileSaverService;
        private IMessageService _messageService;
        private RequestsService _requestsService;


        private string _matlabRecognizeGesture { get; set; } = "recognize";


        #endregion

        #region Public Properties

        public string SwichButtonLabel { get; set; } = "Start recording";
        public bool RecordButtonEnabled { get; set; } = true;
        public string Message { get; set; }
        public IList<Body> Bodies { get; set; }
        public Body Body { get; set; }
        public ObservableCollection<Line> LinesObservable { get; set; }
        public bool RequestProgressBar { get; set; } = false;


        public ICommand SwichButtonCommand { get; set; }
        public ICommand GoToAdminPageCommand { get; set; }

        #endregion

        #region Constructor

        public RecordViewModel()
        {
            KinectHelper.Instance().addObserver(this);

            _messageService = new WindowsMessageService();
            _matFileSaverService = new MatFileSaverService();
            _currentLogin = ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentUser.Login;
            _dataRecorder = new DataRecorderService(_matFileSaverService);
            _drawScaletonService = new DrawScaletonService(_canvasInitHeight,_canvasInitWidth);
            _requestsService = new RequestsService();

            LinesObservable = new ObservableCollection<Line>();

            this.SwichButtonCommand = new RelayCommand(SwitchRecording);
            this.GoToAdminPageCommand = new RelayCommand(GoToAdminPage);
        }

        #endregion

        #region Helper Methods


        private void SwitchRecording()
        {
            if (_recordBody)
            {
                _recordBody = false;
                var matFilePath = _dataRecorder.StopRecording();

                SendRequest(matFilePath);

                SwichButtonLabel = "Start";

            }
            else
            {
                if (Body != null)
                {
                    _recordBody = true;

                    SwichButtonLabel = "Stop";
                    Message = "recording!";
                }
                else
                {
                    _messageService.Show(NO_BODY);
                }                
            }           
        }

        private void GoToAdminPage()
        {
            ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentPage = AppPage.Admin;
        }

        public void updateData()
        {
            //Message = KinectHelper.Message;
            Body = KinectHelper.Body;

            LinesObservable = _drawScaletonService.DrawScaleton(Body);

            if (_recordBody)
            {
                _dataRecorder.StartRecording(Body);
            }
        }

        private async void SendRequest(string matFilePath)
        {
            RecordButtonEnabled = false;
            RequestProgressBar = true;

            ResponseData responseData = await _requestsService.RecognizeGesture(_currentLogin, matFilePath);

            RecordButtonEnabled = true;            
            RequestProgressBar = false;

            if (responseData != null)
            {
                if (responseData.GestureCount <= 15)
                {
                    Message = String.Format("Rozpoznany gest to: {0} Prawdopodobieństwo błędu wynosi {1}%", responseData.GestureName, responseData.GestureCount);
                    var translateToAction = new TranslateRequestToActionService(_messageService);
                    translateToAction.TranslateAndRun(responseData, _currentLogin);
                }
                else
                {
                    Message = String.Format("Niepewność rozpoznania wynosi {0}% ({1})", responseData.GestureCount, responseData.GestureName);
                }
            }
        } 
        #endregion
    }
}

