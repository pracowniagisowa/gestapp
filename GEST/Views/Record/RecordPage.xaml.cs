﻿using GEST.ViewModels;
using System.Windows.Controls;

namespace GEST.Views
{
    /// <summary>
    /// Interaction logic for RecordPage.xaml
    /// </summary>
    public partial class RecordPage : Page
    {
        public RecordPage()
        {
            InitializeComponent();
            this.DataContext = new RecordViewModel();
        }
    }
}
