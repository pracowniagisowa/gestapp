﻿using System.Windows.Controls;

namespace GEST.Views
{
    /// <summary>
    /// Interaction logic for NewUserPage.xaml
    /// </summary>
    public partial class NewUserPage : Page
    {
        public NewUserPage()
        {
            InitializeComponent();
            this.DataContext = new NewUserViewModel();
        }
    }
}
