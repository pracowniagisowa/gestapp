﻿using GEST.ViewModels;
using KinectReader;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace GEST.Views
{
    class NewUserViewModel : BaseViewModel
    {
        #region Private Properties

        private string _loginExistsInDb { get; set; } = "Podany login znajduje się w bazie danych";
        private string _passwordsNotEqual { get; set; } = "Podane hasła się nie zgadzają";
        private string _dataNotCopleted { get; set; } = "Uzupełnij wszytkie dane";
        private string _addedToDb { get; set; } = "Dodano użytkownika do bazy danych";
    

        private GestDbContext _db { get; set; }

        #endregion

        #region Public Properties

        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string RepeatPassword { get; set; }
        
        public ICommand AddNewUserCommand { get; set; }

        #endregion

        #region Constructor

        public NewUserViewModel()
        {
            _db = new GestDbContext();
            this.AddNewUserCommand = new RelayCommand(AddNewUser);

        }

        #endregion

        #region Helper Methods

        private void AddNewUser()
        {
            if (_db.GestUsers.FirstOrDefault(u => u.Login == Login) == null)
            {
                //MessageBox.Show("Czas dodac coś do bazy danych :)");
                if (Name != null && Login != null && Password != null)
                {
                    if (Password == RepeatPassword)
                    {
                        GestUser newUser = new GestUser(Name, Login, Password);
                        //TODO: zabezpieczenie przed zdublowanym loginem
                        _db.GestUsers.Add(newUser);
                        _db.SaveChanges();
                        ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentUser = newUser;
                        MessageBox.Show(_addedToDb);
                        ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentPage = AppPage.Record;
                    }
                    else
                    {
                        MessageBox.Show(_passwordsNotEqual);
                    }
                }
                else
                {
                    MessageBox.Show(_dataNotCopleted);
                }              
                
            }
            else
            {
                MessageBox.Show(_loginExistsInDb);
            }
        }

        #endregion
    }
}
