﻿using System.Windows.Controls;

namespace GEST.Views.Admin
{
    /// <summary>
    /// Interaction logic for AdminPage.xaml
    /// </summary>
    public partial class AdminPage : Page
    {
        public AdminPage()
        {
            InitializeComponent();
            DataContext = new AdminPageViewModel();
        }
    }
}
