﻿using GEST.DataModels;
using GEST.Helpers;
using GEST.Services;
using GEST.ViewModels;
using KinectReader;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace GEST.Views.Admin
{
    class AdminPageViewModel : BaseViewModel
    {
        #region Private Properties

        private string _currentLogin { get; set; }
        private GestDbContext _db;
        private List<Gesture> _gesturesList { get; set; }
        private Gesture _selectedGesture;
        private IMessageService _messageService { get; set; }

        private string MATLAB_UPDATE_MODEL { get; set; } = "updateModels";
        private string _loginExistsInDb { get; set; } = "Podany login znajduje się w bazie danych";
        private string _passwordsNotEqual { get; set; } = "Podane hasła się nie zgadzają";
        private string _dataNotCopleted { get; set; } = "Uzupełnij wszytkie dane";
        private string _addedToDb { get; set; } = "Dodano użytkownika do bazy danych";
        private string _firstSelectRow { get; set; } = "Wybierz (kliknij) gest w tabeli";
        private string _changesSaved { get; set; } = "Zmiany zapisane";
        private string _gestureAdded { get; set; } = "Gest dodany do polecenia";
        private string _gestureRemoved { get; set; } = "Polecenie usunięte";

        #endregion

        #region Public Properties

        public ObservableCollection<Gesture> GesturesCollection { get; set; }
        //public Gesture SelectedGesture { get; set; }
        public ActionService ActionService { get; set; }
        public List<string> ActionsList { get; set; }
        public string ChosenAction { get; set; }
        public bool RequestProgressBar { get; set; } = false;
        public bool UpdateModelButtonEnabled { get; set; } = true;
        public bool AddCommandButtonEnabled { get; set; } = true;
        public bool TableButtonsEnabled { get; set; } = true;


        public Gesture SelectedGesture
        {
            get { return _selectedGesture; }
            set {
                _selectedGesture = value;
                ShareObject.Instance().CurrentGesture = value;
            }
        }

        public ICommand GoToRecordPageCommand { get; set; }
        public ICommand AddOrderCommand { get; set; }
        public ICommand SaveEditsCommand { get; set; }
        public ICommand AddGestureToOrderCommand { get; set; }
        public ICommand RemoveOrderCommand { get; set; }
        public ICommand UpdateModelCommand { get; set; }
        
        #endregion

        #region Constructor

        public AdminPageViewModel()
        {
            _db = new GestDbContext();
            _messageService = new WindowsMessageService();


            _currentLogin = ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentUser.Login;
            GoToRecordPageCommand = new RelayCommand(GoToRecordPage);
            AddOrderCommand = new RelayCommand(AddOrder);
            SaveEditsCommand = new RelayCommand(SaveEdits);
            UpdateModelCommand = new RelayCommand(UpdateModel);
            AddGestureToOrderCommand = new RelayCommand(AddGestureToOrder);
            RemoveOrderCommand = new RelayCommand(RemoveOrder);

            var currentUser = _db.GestUsers.FirstOrDefault(u => u.Login == _currentLogin);
            _gesturesList = currentUser.Gestures.ToList();
            GesturesCollection = new ObservableCollection<Gesture>(_gesturesList);
            ActionService = new ActionService();
            ActionsList = ActionService.ActionsList;
        }

        #endregion

        #region Helper Methods

        private void UpdateDatagrid()
        {
            using (var db = new GestDbContext())
            {
                GesturesCollection = new ObservableCollection<Gesture>(db.GestUsers.FirstOrDefault(u => u.Login == _currentLogin).Gestures);
            }
        }

        private void AddOrder()
        {
            Gesture newGesture = new Gesture()
            {
                Action = "wybierz akcję",
            };
            using (var db = new GestDbContext())
            {
                GestUser currentUser = db.GestUsers.FirstOrDefault(u => u.Login == _currentLogin);

                currentUser.Gestures.Add(newGesture);
                db.SaveChanges();
            }
            UpdateDatagrid();
            
        }

        private void SaveEdits()
        {
            //https://stackoverflow.com/questions/21582253/wpf-mvvm-update-datagrid-when-combobox-value-changes
            var selectedGesture = SelectedGesture;
            if (selectedGesture == null)
            {
                _messageService.Show(_firstSelectRow);
            }
            else
            {
                using (var db = new GestDbContext())
                {
                    Gesture gestureInDb = db.Gestures.FirstOrDefault(g => g.Id == selectedGesture.Id);
                    gestureInDb.GestureName = selectedGesture.GestureName;
                    gestureInDb.Action = selectedGesture.Action;
                    db.SaveChanges(); 
                }
                UpdateDatagrid();
                _messageService.Show(_changesSaved);

            }                                   
        }

        public void AddGestureToOrder()
        {
            var selectedGesture = SelectedGesture;
            if (selectedGesture == null)
            {
                _messageService.Show(_firstSelectRow);
            }
            else
            {
                ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentPage = AppPage.AddGesture;
            }
        }

        public void RemoveOrder()
        {
            var selectedGesture = SelectedGesture;
            if (selectedGesture == null)
            {
                _messageService.Show(_firstSelectRow);
            }
            else
            {
                using (var db = new GestDbContext())
                {
                    Gesture gestureInDb = db.Gestures.FirstOrDefault(g => g.Id == selectedGesture.Id);
                    db.Gestures.Remove(gestureInDb);
                    db.SaveChanges();                    
                }
                UpdateDatagrid();
                _messageService.Show(_gestureRemoved);
            }            
        }

        private async void UpdateModel()
        {
            RequestProgressBar = true;
            BlockUI();

            RequestData requestData = new RequestData()
            {
                Action = MATLAB_UPDATE_MODEL,
                UserName = _currentLogin,
                GestureName = "aaa",
                MatFileFullPath = "aaa"
            };

            IMatlabService matlabService = new GestMatlabService();
            var matlabOutcome = await matlabService.OutcomeAsync(requestData);
            RequestProgressBar = false;
            UnBlockUI();

            if (matlabOutcome.GestureOk)
            {
                _messageService.Show("Zaktualizowano");
            }
            else
            {
                _messageService.Show("nie zaktualizowano.....");
            }
        }

        private void GoToRecordPage()
        {
            ((MainWindowViewModel)((MainWindow)App.Current.MainWindow).DataContext).CurrentPage = AppPage.Record;
        }

        private void BlockUI()
        {
            UpdateModelButtonEnabled = false;
            AddCommandButtonEnabled = false;
            TableButtonsEnabled = false;
        }

        private void UnBlockUI()
        {
            UpdateModelButtonEnabled = true;
            AddCommandButtonEnabled = true;
            TableButtonsEnabled = true;
        }

        #endregion
    }
}
