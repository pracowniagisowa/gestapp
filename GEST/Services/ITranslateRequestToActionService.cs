﻿using GEST.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Services
{
    interface ITranslateRequestToActionService
    {
        void TranslateAndRun(ResponseData responseData, string userLogin);
    }
}
