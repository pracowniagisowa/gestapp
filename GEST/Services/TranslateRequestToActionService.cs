﻿using GEST.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Services
{
    class TranslateRequestToActionService : ITranslateRequestToActionService
    {
        private string _gestureNotOk { get; set; } = "Gest niewłaściwy, proszę powtórz go";

        private IMessageService _messageService;

        public TranslateRequestToActionService(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public void TranslateAndRun(ResponseData responseData, string userLogin)
        {
            if (responseData.GestureOk)
            {
                ActionService actionService = new ActionService();
                using (var db = new GestDbContext())
                {
                    GestUser currentUser = db.GestUsers.FirstOrDefault(u => u.Login == userLogin);
                    Gesture recognizedGesture = currentUser.Gestures.FirstOrDefault(g => g.GestureName == responseData.GestureName);

                    actionService.RunAction(recognizedGesture.Action);
                }
            }
            else
            {
                _messageService.Show(_gestureNotOk);
            }

            
        }

    }
}
