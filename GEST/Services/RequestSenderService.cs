﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Services
{
    class RequestSenderService
    {
        private List<Body> _bodiesList;

        public List<Body> BodiesList
        {
            get { return _bodiesList; }
            set { _bodiesList = value; }
        }

        private List<float[]> _bodyData;

        public List<float[]> BodyData
        {
            get { return _bodyData; }
            set { _bodyData = value; }


        }


    }
}
