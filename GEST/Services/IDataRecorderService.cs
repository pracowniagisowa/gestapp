﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Services
{
    public interface IDataRecorderService
    {
        void StartRecording(Body body);
        //void StartRecordingAll(Body body);
        string StopRecording();
    }
}
