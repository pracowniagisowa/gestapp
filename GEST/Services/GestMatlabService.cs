﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEST.DataModels;

namespace GEST.Services
{
    class GestMatlabService : IMatlabService
    {
        private string _appDataPath = @"C:\Users\MF\Dropbox\GestApp";
        //private string appDataPath = @"C:\Dane\Git\Alpha\Toolbox_alpha\GestApp";
        private string _matlabScriptName = "test_dataflow";

        private IMessageService _messageService;

        public GestMatlabService()
        {
            _messageService = new WindowsMessageService();
        }

        public ResponseData Outcome(RequestData requestData)
        {
            object result = null;

            MLApp.MLApp matlab = new MLApp.MLApp();
            matlab.Execute(@"cd " + _appDataPath);

            matlab.Feval(_matlabScriptName, 4, out result, 
                requestData.Action, 
                requestData.UserName, 
                requestData.GestureName,
                requestData.MatFileFullPath);

            object[] res = result as object[];
            var test = res[2];
            var test2 = res[2].ToString();
            ResponseData responseData = new ResponseData()
            {
                GestureName = res[0].ToString(),
                UserName = res[1].ToString(),
                GestureOk = res[2].ToString() == "True",
                GestureCount = Int32.Parse(res[3].ToString())
            };
            return responseData;
        }

        public Task<ResponseData> OutcomeAsync(RequestData requestData)
        {
            return Task.Run(() => Outcome(requestData));
        }
                
    }
}
