﻿using GEST.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Services
{
    interface IMatlabService
    {
        Task<ResponseData> OutcomeAsync(RequestData requestData);
        //string Outcome(string jsonString);
        ResponseData Outcome(RequestData requestData);

    }
}
