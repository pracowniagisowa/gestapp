﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows;

namespace GEST.Services
{
    class ActionService
    {
        #region PublicProperties

        public List<string> ActionsList { get; set; }
        //TODO: przerobić ActionsList na Enum
        public static class Actions
        {
            public static readonly string Mail = "gmail";
            public static readonly string Weather = "pogoda";
            public static readonly string News = "wiadomosci";
            public static readonly string PickAction = "wybierz akcję";
        }

        #endregion

        #region Ctor
        public ActionService()
        {
            ActionsList = new List<string>()
            {
                "gmail",
                "pogoda",
                "wiadomosci",
                "wybierz akcję"
            };
        }

        #endregion

        #region PublicMethods
        public void RunAction(string actionName)
        {
            switch (actionName)
            {
                case "gmail":
                    RunGmail();
                    break;
                case "pogoda":
                    RunMMeteo();
                    break;
                case "wiadomosci":
                    runNews();
                    break;
                default:
                    MessageBox.Show("brak przypisanej akcji");
                    break;
            }
        }

        #endregion

        #region PrivateMethods
        private void RunGmail()
        {
            Process.Start("http://www.gmail.com");
        }

        private void RunMMeteo()
        {
            Process.Start("http://m.meteo.pl/krakow/60");
        }

        private void runNews()
        {
            Process.Start("http://www.polskieradio.pl/5,Wiadomosci");
        }


        #endregion
    }
}

