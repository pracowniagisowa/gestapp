﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Services
{
    public class MatFileSaverService : IMatFileSaverService
    {
        public void Save(float[][] bufferBody, string fullPath)
        {
            var test = bufferBody[0];
            if (bufferBody != null)
            {
                csmatio.types.MLSingle bodyDataMat = new csmatio.types.MLSingle("bodyData", bufferBody);
                List<csmatio.types.MLArray> lstBodyDataMat = new List<csmatio.types.MLArray>();
                lstBodyDataMat.Add(bodyDataMat);
                csmatio.io.MatFileWriter writer = new csmatio.io.MatFileWriter(fullPath, lstBodyDataMat, true);
                bufferBody = null;
            }
        }
    }
}
