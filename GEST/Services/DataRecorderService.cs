﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GEST.Services
{
    public class DataRecorderService : IDataRecorderService
    {
        private bool recordAllGestures = false;

        private List<float[]> _bodyDataBuffor;
        private IMatFileSaverService _matFileSaver;
        private IMessageService _messageService;

        private List<string> _jointsList = new List<string>()
        {
            "Head",
            "ShoulderRight",
            "ElbowRight",
            "WristRight",
            "HandRight",
            "HandTipRight",
            "ThumbRight"
        };

        public DataRecorderService(IMatFileSaverService matFileSaver)
        {
            _bodyDataBuffor = new List<float[]>();
            _matFileSaver = matFileSaver;
            _messageService = new WindowsMessageService();
        }

        public void StartRecording(Body body)
        {
            if (recordAllGestures)
            {
                StartRecordingAll(body);
            }
            else
            {
                StartRecordingSelected(body);
            }
        }

        public void StartRecordingSelected(Body body)
        {
            int jointsListCount = _jointsList.Count;
            float[] _bodyData = new float[jointsListCount * 8];
            int idx = 0;

            foreach (var kvp in body.Joints)
            {
                var jointTypeString = kvp.Key.ToString();
                if (_jointsList.Contains(jointTypeString))
                {
                    var jointType = kvp.Key;
                    var joint = kvp.Value;
                    var jointOrientation = body.JointOrientations[jointType];

                    CameraSpacePoint position = joint.Position;
                    if (position.Z < 0) position.Z = 0.1f;          // according to Kinect code sample (sometimes z < 0 and the mapping return -infinity)


                    _bodyData[idx++] = (int)joint.TrackingState;
                    _bodyData[idx++] = joint.Position.X;
                    _bodyData[idx++] = joint.Position.Y;
                    _bodyData[idx++] = joint.Position.Z;
                    _bodyData[idx++] = jointOrientation.Orientation.X;
                    _bodyData[idx++] = jointOrientation.Orientation.Y;
                    _bodyData[idx++] = jointOrientation.Orientation.Z;
                    _bodyData[idx++] = jointOrientation.Orientation.W;
                }
            }
            _bodyDataBuffor.Add(_bodyData);

        }

        public void StartRecordingAll(Body body)
        {
            int jointsListCount = body.Joints.Count;
            float[] _bodyData = new float[jointsListCount * 8];
            int idx = 0;

            foreach (var kvp in body.Joints)
            {
                var jointType = kvp.Key;
                var joint = kvp.Value;
                var jointOrientation = body.JointOrientations[jointType];

                CameraSpacePoint position = joint.Position;
                if (position.Z < 0) position.Z = 0.1f;          // according to Kinect code sample (sometimes z < 0 and the mapping return -infinity)

                //DepthSpacePoint depthSpacePoint = coordMapper.MapCameraPointToDepthSpace(position);
                //ColorSpacePoint colorSpacePoint = coordMapper.MapCameraPointToColorSpace(position);

                _bodyData[idx++] = (int)joint.TrackingState;
                _bodyData[idx++] = joint.Position.X;
                _bodyData[idx++] = joint.Position.Y;
                _bodyData[idx++] = joint.Position.Z;
                //_bodyData[idx++] = depthSpacePoint.X;
                //_bodyData[idx++] = depthSpacePoint.Y;
                //_bodyData[idx++] = colorSpacePoint.X;
                //_bodyData[idx++] = colorSpacePoint.Y;
                _bodyData[idx++] = jointOrientation.Orientation.X;
                _bodyData[idx++] = jointOrientation.Orientation.Y;
                _bodyData[idx++] = jointOrientation.Orientation.Z;
                _bodyData[idx++] = jointOrientation.Orientation.W;
            }
            _bodyDataBuffor.Add(_bodyData);

        }


        public string StopRecording()
        {

            Guid g = Guid.NewGuid();
            //string fullPath = @"C:\Users\MF\KinectRecorder\" + g.ToString() + ".mat";
            string localData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string dirPath = @"\MatData";
            string fileName = g.ToString() + ".mat";

            string fullPath = Path.Combine(localData + dirPath, fileName);

            DirectoryInfo dir = Directory.CreateDirectory(localData + dirPath);

            if (_bodyDataBuffor.Count > 0)
            {
                float[][] bodyData = _bodyDataBuffor.ToArray();
                _matFileSaver.Save(bodyData, fullPath);
                _bodyDataBuffor = new List<float[]>();
            }
            else
            {
                var _bodyDataBuffor = new List<float[]>
                {
                    new float[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                    new float[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },

                };

                float[][] bodyData = _bodyDataBuffor.ToArray();
                _matFileSaver.Save(bodyData, fullPath);
                _bodyDataBuffor = new List<float[]>();                
            }
            return fullPath;     
        }
    }
}