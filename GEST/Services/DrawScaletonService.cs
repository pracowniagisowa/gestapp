﻿using GEST.DataModels;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GEST.Services
{
    class DrawScaletonService
    {
        
        public int CanvasInitHeight { get; set; } = 268;
        public int CanvasInitWidth { get; set; } = 580;

        private List<Tuple<string, string>> JointTypePairs = new List<Tuple<string, string>>()
        {
            Tuple.Create("Head", "Neck"),
            Tuple.Create("Neck", "SpineShoulder"),
            Tuple.Create("SpineShoulder", "ShoulderLeft"),
            Tuple.Create("SpineShoulder", "ShoulderRight"),
            Tuple.Create("SpineShoulder", "SpineMid"),
            Tuple.Create("ShoulderLeft", "ElbowLeft"),
            Tuple.Create("ShoulderRight", "ElbowRight"),
            Tuple.Create("ElbowLeft", "WristLeft"),
            Tuple.Create("ElbowRight", "WristRight"),
            Tuple.Create("WristLeft", "HandLeft"),
            Tuple.Create("WristRight", "HandRight"),
            Tuple.Create("HandLeft", "HandTipLeft"),
            Tuple.Create("HandRight", "HandTipRight"),
            Tuple.Create("HandTipLeft", "ThumbLeft"),
            Tuple.Create("HandTipRight", "ThumbRight"),
            Tuple.Create("SpineMid", "SpineBase"),
            Tuple.Create("SpineBase", "HipLeft"),
            Tuple.Create("SpineBase", "HipRight"),
            Tuple.Create("HipLeft", "KneeLeft"),
            Tuple.Create("HipRight", "KneeRight"),
            Tuple.Create("KneeLeft", "AnkleLeft"),
            Tuple.Create("KneeRight", "AnkleRight"),
            Tuple.Create("AnkleLeft", "FootLeft"),
            Tuple.Create("AnkleRight", "FootRight")
        };  

        #region ctor

        public DrawScaletonService(int canvasInitHeight, int canvasInitWidth)
        {
            CanvasInitHeight = canvasInitHeight;
            CanvasInitWidth = canvasInitWidth;
        }

        #endregion

        #region Public Methods

        public ObservableCollection<Line> DrawScaleton(Body body)
        {
            ObservableCollection<Line> scaletonLines = new ObservableCollection<Line>();

            try
            {
                scaletonLines.Add(DrawLine(body.Joints[JointType.Head], body.Joints[JointType.Neck]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.Neck], body.Joints[JointType.SpineShoulder]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.SpineShoulder], body.Joints[JointType.ShoulderLeft]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.SpineShoulder], body.Joints[JointType.ShoulderRight]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.SpineShoulder], body.Joints[JointType.SpineMid]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.ShoulderLeft], body.Joints[JointType.ElbowLeft]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.ShoulderRight], body.Joints[JointType.ElbowRight]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.ElbowLeft], body.Joints[JointType.WristLeft]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.ElbowRight], body.Joints[JointType.WristRight]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.WristLeft], body.Joints[JointType.HandLeft]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.WristRight], body.Joints[JointType.HandRight]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.HandLeft], body.Joints[JointType.HandTipLeft]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.HandRight], body.Joints[JointType.HandTipRight]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.HandTipLeft], body.Joints[JointType.ThumbLeft]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.HandTipRight], body.Joints[JointType.ThumbRight]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.SpineMid], body.Joints[JointType.SpineBase]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.SpineBase], body.Joints[JointType.HipLeft]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.SpineBase], body.Joints[JointType.HipRight]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.HipLeft], body.Joints[JointType.KneeLeft]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.HipRight], body.Joints[JointType.KneeRight]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.KneeLeft], body.Joints[JointType.AnkleLeft]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.KneeRight], body.Joints[JointType.AnkleRight]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.AnkleLeft], body.Joints[JointType.FootLeft]));
                scaletonLines.Add(DrawLine(body.Joints[JointType.AnkleRight], body.Joints[JointType.FootRight]));
            }
            catch (Exception)
            {
                
            }
            

            return scaletonLines;
    }

        #endregion

        #region Private Methods

        private Line DrawLine(Joint first, Joint second)
        {
            first = first.ScaleTo(CanvasInitWidth, CanvasInitHeight);
            second = second.ScaleTo(CanvasInitWidth, CanvasInitHeight);

            Line line = new Line
            {
                X1 = first.Position.X,
                Y1 = first.Position.Y,
                X2 = second.Position.X,
                Y2 = second.Position.Y,
                StrokeThickness = 8,
                Stroke = new SolidColorBrush(Colors.LightBlue)
            };

            return line; 
        }

        

        #endregion
    }
}
