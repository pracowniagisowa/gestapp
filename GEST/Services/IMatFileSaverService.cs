﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Services
{
    public interface IMatFileSaverService
    {
        void Save(float[][] bufferBody, string fullPath);
    }
}
