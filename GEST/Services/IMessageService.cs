﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Services
{
    public interface IMessageService
    {
        void Show(string message);
        bool ShowAndAsk(string message);
    }
}
