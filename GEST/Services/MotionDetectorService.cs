﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Services
{
    class MotionDetectorService
    {
        private List<float> _motionData;
        private int _motionDataLen = 3;

        public MotionDetectorService()
        {
            _motionData = new List<float>();
        }

        public float Update(Body body)
        {
            float centralDifference = 0;
            if (body.Joints.Count > 10)
            {
                float wirstRightY = body.Joints[(JointType)9].Position.Y;
                if (_motionData.Count < _motionDataLen)
                {
                    _motionData.Add(wirstRightY);
                }
                else
                {
                    _motionData.RemoveAt(0);
                    _motionData.Add(wirstRightY);
                    // funkcja dla listy trzech wartości
                    centralDifference = ((_motionData[_motionData.Count / 2] - _motionData[0]) + (_motionData[_motionData.Count - 1] - _motionData[_motionData.Count / 2])) / 2;
                }
            }
            return centralDifference;

        }



    }
}
