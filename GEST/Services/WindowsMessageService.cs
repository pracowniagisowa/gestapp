﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GEST.Services
{
    public class WindowsMessageService : IMessageService
    {
        public void Show(string message)
        {
            MessageBox.Show(message);
        }

        public bool ShowAndAsk(string message)
        {
            MessageBoxResult userDecision = MessageBox.Show(message, "OK", MessageBoxButton.YesNo);
            return userDecision == MessageBoxResult.Yes;
        }


    }
}
