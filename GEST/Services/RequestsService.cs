﻿using GEST.DataModels;
using GEST.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Services
{
    class RequestsService
    {
        private const string MATLAB_RECOGNIZE_GESTURE = "recognize";
        private const string MATLAB_ADD_GESTURE = "addGesture";
        private const string MATLAB_UPDATE_MODEL = "updateModels";

        //private string _currentLogin { get; set; }

        public RequestsService()
        {
            
        }

        public async Task<ResponseData> RecognizeGesture(string currentLogin, string matFilePath)
        {
            RequestData requestData = new RequestData()
            {
                Action = MATLAB_RECOGNIZE_GESTURE,
                UserName = currentLogin,
                MatFileFullPath = matFilePath,
                GestureName = "X"
            };

            IMatlabService matlabService = new GestMatlabService();
            ResponseData responseData =await matlabService.OutcomeAsync(requestData);//.ContinueWith(AfterRequest);
            return responseData;
        }

        public async Task<ResponseData> AddGesture(string currentLogin, string matFilePath, string gestureName)
        {
            RequestData requestData = new RequestData()
            {
                Action = MATLAB_ADD_GESTURE,
                UserName = currentLogin,
                GestureName = gestureName,
                MatFileFullPath = matFilePath
            };

            IMatlabService matlabService = new GestMatlabService();
            ResponseData responseData = await matlabService.OutcomeAsync(requestData);//.ContinueWith(AfterRequest);
            return responseData;
        }

        public async Task<ResponseData> UpdateModels(string currentLogin)
        {
            RequestData requestData = new RequestData()
            {
                Action = MATLAB_UPDATE_MODEL,
                UserName = currentLogin,
                GestureName = "aaa",
                MatFileFullPath = "aaa"
            };

            IMatlabService matlabService = new GestMatlabService();
            ResponseData responseData = await matlabService.OutcomeAsync(requestData);//.ContinueWith(AfterRequest);
            return responseData;
        }
    }
}
