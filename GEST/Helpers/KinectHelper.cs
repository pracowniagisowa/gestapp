﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using KinectReader;
using System.ComponentModel;
using GEST.Helpers;
using GEST.Views;
using System.Timers;

namespace KinectReader.Models.Helpers
{
    class KinectHelper : IObserved
    {
        private static KinectHelper _instance;
        private List<IObserver> _observatorsList = new List<IObserver>();
        private IObserver observer;

        private static KinectSensor _kinect;
        private static MultiSourceFrameReader _reader;
        //private static IList<Body> _bodies;
        //private static MotionDetector _motionDetector; 

        public static IList<Body> Bodies { get; set; }
        public static Body Body { get; set; }
        public static string Message { get; set; }
        public Timer timer { get; set; } = new Timer();
        public int Count { get; set; }

        // konstruktor:
        private KinectHelper()
        {
            _kinect = KinectSensor.GetDefault();

            if (_kinect != null)
            {
                //turn on kinect
                _kinect.Open();
                Console.WriteLine("kinect opened");
                _reader = _kinect.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.Body);
                _reader.MultiSourceFrameArrived += Reader_MultiSourceFrameArrived;
            }
            else
            {
                throw new Exception("Kinect not connected");
            }

        }

        public static KinectHelper Instance()
        {
            if (_instance == null)
            {
                _instance = new KinectHelper();
            }
            return _instance;
        }

        public void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            var reference = e.FrameReference.AcquireFrame();


            // Body
            using (var frame = reference.BodyFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    Bodies = new Body[frame.BodyFrameSource.BodyCount];
                    frame.GetAndRefreshBodyData(Bodies);
                    Body = Bodies.FirstOrDefault(b => b.Lean.X != 0);

                    if (Body != null)
                    {
                        var x = Body.Joints[(JointType)9].Position.X;
                        var y = Body.Joints[(JointType)9].Position.Y;
                        var z = Body.Joints[(JointType)9].Position.Z;
                        var formatedString = String.Format("X={0}; Y={1}; Z={2}", x, y, z);
                        Console.WriteLine(formatedString);
                        Message = formatedString;
                    }

                    
                    notifyObservers();
                }
            }
        }

        public void Counter()
        {
            timer.Interval = 500;
            timer.Elapsed += new ElapsedEventHandler(Tick);
            timer.Start();
        }

        private void Tick(object sender, EventArgs e)
        {
            Count += 1;
            Message = Count.ToString();
            notifyObservers();
        }

        public void addObserver(IObserver o)
        {
            observer = o;
            //if (_observatorsList.FirstOrDefault(v => v.GetType() == o.GetType()) == null)
            //{
            //    _observatorsList.Add(o); 
            //}
        }

        public void delObserver(IObserver o)
        {
            if (!_observatorsList.Contains(o))
            {
                _observatorsList.Remove(o);
            }

        }

        public void notifyObservers()
        {
            observer.updateData();
            //foreach (var observer in _observatorsList)
            //{
            //    observer.updateData();
            //}
        }
    }
}
