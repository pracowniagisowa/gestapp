﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Helpers
{
    interface IObserved
    {
        void addObserver(IObserver o);
        void delObserver(IObserver o);
        void notifyObservers();
    }
}
                                                                                            