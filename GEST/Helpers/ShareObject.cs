﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.Helpers
{
    class ShareObject
    {
        #region Privates
        private static ShareObject _instance { get; set; }
        #endregion

        #region Publics
        public Gesture CurrentGesture { get; set; }

        #endregion


        #region Ctor
        private ShareObject()
        {

        }
        #endregion

        #region Methods
        public static ShareObject Instance()
        {
            if (_instance == null)
            {
                _instance = new ShareObject(); 
            }
            return _instance;

        }
        #endregion
    }
}
                                  