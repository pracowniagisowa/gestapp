﻿using GEST.Views;
using PropertyChanged;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GEST.ViewModels
{
    //[ImplementPropertyChanged]
    public class MainWindowViewModel : BaseViewModel
    {
        #region Private Propserties


        #endregion

        #region Public Properties

        public AppPage CurrentPage { get; set; } = AppPage.Login;

        public GestUser CurrentUser { get; set; }

        #endregion

        #region Constructor
        public MainWindowViewModel()
        {
            //var context = new GestDbContext();
            //var entity = context.Gestures.ToString();
        }

        #endregion

        #region Helper Methods


        #endregion

        
    }
}
