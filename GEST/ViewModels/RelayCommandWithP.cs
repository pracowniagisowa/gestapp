﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GEST.ViewModels
{
    class RelayCommandWithP<T> : ICommand
    {
        private Action<T> mAction;

        public event EventHandler CanExecuteChanged = (sender, e) => { };

        public RelayCommandWithP(Action<T> action)
        {
            mAction = action;            
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            mAction((T)parameter);
        }
    }
}
