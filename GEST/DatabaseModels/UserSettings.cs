﻿using System.ComponentModel.DataAnnotations;

namespace GEST
{
    class UserSettings
    {
        [Key]
        public int Id { get; set; }
        public string CurrentLogin { get; set; }
        public string CurrentPassword { get; set; }


    }
}
