﻿using System.ComponentModel.DataAnnotations;

namespace GEST
{
    public class Gesture
    {
        public int Id { get; set; }
        [StringLength(255)]
        public string GestureName { get; set; }
        [StringLength(255)]
        public string Action { get; set; }
        public int Quantity { get; set; }

    }
}
