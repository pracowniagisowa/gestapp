﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace GEST
{
    public class GestUser
    {
        public int Id { get; set; }
        [Required]
        [StringLength(70)]
        public string Login { get; set; }
        [StringLength(70)]
        public string Name { get; set; }
        [StringLength(250)]
        public string Password { get; set; }
        public virtual ICollection<Gesture> Gestures { get; set; }

        public GestUser()
        {
            Gestures = new List<Gesture>();
        }

        public GestUser(string name, string login, string password)
        {
            Name = name;
            Login = login;
            Password = password;
            Gestures = new List<Gesture>();
        }
    }
}
