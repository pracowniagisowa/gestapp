﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST
{
    //[DbConfigurationType(typeof(GestDbConfiguration))]
    class GestDbContext : DbContext
    {
        public GestDbContext()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<GestDbContext>());
        }

        public DbSet<GestUser> GestUsers { get; set; }
        public DbSet<Gesture> Gestures { get; set; }
        public DbSet<UserSettings> UserSettings { get; set; }


        public static GestDbContext Create()
        {                                 
            return new GestDbContext();
        }
    }
}
