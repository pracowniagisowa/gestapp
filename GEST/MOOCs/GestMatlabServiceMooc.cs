﻿using GEST.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEST.DataModels;

namespace GEST.MOOCs
{
    class GestMatlabServiceMooc : IMatlabService
    {
        public ResponseData Outcome(RequestData requestData)
        {
            ResponseData responseData = new ResponseData();
            responseData.GestureName = requestData.GestureName;
            responseData.GestureOk = true;
            responseData.UserName = requestData.UserName;
            using (var db = new GestDbContext())
            {
                Gesture recognizedGesture = db.Gestures.FirstOrDefault(g => g.GestureName == responseData.GestureName);
                responseData.GestureCount = recognizedGesture.Quantity + 1;
            }

            return responseData;
        }

        public Task<ResponseData> OutcomeAsync(RequestData requestData)
        {
            return Task.Run(() => Outcome(requestData));
        }
    }
}
