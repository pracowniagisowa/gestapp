﻿using GEST.ViewModels;
using MahApps.Metro.Controls;

namespace KinectReader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainWindowViewModel();

        }

        //private KinectHelper _kinectHelper;// = new KinectHelper();

        //public MainWindow()
        //{
        //    InitializeComponent();
        //    //var dialog = new E
        //    _kinectHelper = new KinectHelper();
        //    // copy our model object state to the UI
        //    MessageBlock.Text = _kinectHelper.Message;
        //    // 'listen' to changes in the model
        //    _kinectHelper.PropertyChanged += Event_PropertyChanged; 
        //}

        //private void Event_PropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName == "Message")
        //    {
        //        MessageBlock.Text = _kinectHelper.Message;

        //    }
        //}
        //private void EventMessage_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    _kinectHelper.Message = MessageBlock.Text;
        //}





        //private void SwitchButton_Click(object sender, RoutedEventArgs e)
        //{
        //    bool recordBody = _kinectHelper.RecordBody;
        //    if (recordBody)
        //    {
        //        _kinectHelper.RecordBody = false;
        //        _kinectHelper.DataRecorder.StopRecording();
        //        switchButton.Content = "Start recording";
        //    }
        //    else
        //    {
        //        _kinectHelper.RecordBody = true;

        //        switchButton.Content = "Stop recording";
        //    }

        //}


    }
}
