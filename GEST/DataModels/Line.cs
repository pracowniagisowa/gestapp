﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GEST.DataModels
{
    class Line
    {
        public float X1 { get; set; }
        public float Y1 { get; set; }
        public float X2 { get; set; }
        public float Y2 { get; set; }
        public int StrokeThickness { get; set; }
        public SolidColorBrush Stroke { get; set; }
    }
}
