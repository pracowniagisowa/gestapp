﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.DataModels
{
    class RequestData
    {
        public string Action { get; set; }
        public string UserName { get; set; }
        public string GestureName { get; set; }
        public string MatFileFullPath { get; set; }
    }
}
