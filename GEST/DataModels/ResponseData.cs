﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.DataModels
{
    class ResponseData
    {
        public string GestureName { get; set; }
        public string UserName { get; set; }
        public bool GestureOk { get; set; }
        public int GestureCount { get; set; }
    }
}
