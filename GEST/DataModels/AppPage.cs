﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST
{
    public enum AppPage
    {
        Login = 0,
        Record = 1,
        NewUser = 3,
        Admin = 4,
        AddGesture = 5,
    }
}
