﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GEST.DataModels
{
    class Ellipse
    {
        public float X1 { get; set; }
        public float Y1 { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public SolidColorBrush Fill { get; set; }

    }
}
