﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEST.DataModels.Enums
{
    enum Visibility
    {
        Visible = 0,
        Collapsed = 1
    }
}
