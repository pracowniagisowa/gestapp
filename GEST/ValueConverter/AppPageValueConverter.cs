﻿
using GEST.ValueConverter;
using GEST.Views;
using GEST.Views.AddGesture;
using GEST.Views.Admin;
using System;
using System.Diagnostics;
using System.Globalization;

namespace GEST
{
    public class AppPageValueConverter : BaseValueConverter<AppPageValueConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((AppPage)value)
            {
                case AppPage.Login:
                    return new LoginPage();

                case AppPage.Record:
                    return new RecordPage();

                case AppPage.NewUser:
                    return new NewUserPage();

                case AppPage.Admin:
                    return new AdminPage();

                case AppPage.AddGesture:
                    return new AddGesture();

                default:
                    Debugger.Break();
                    return null;

            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
