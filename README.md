# GestApp 1.0 - w rozwoju #

Aplikacja umożliwia interakcję użytkownika z komputerem z wykorzystaniem gestów. Przy pomocy aplikacji użytkownik nagrywa własne gesty, nazywa je, a także przypisuje im akcje, jakie mają być wykonane przez komputer. Urządzeniem wykorzystanym do akwizycji danych jest sensor Microsoft Kinect. Aplikacja komunikuje się z serwerem (Matlab), na którym następuje analiza i automatyczne rozpoznawanie gestów.  Na podstawie otrzymanej informacji na temat wyników rozpoznawani, aplikacja inicjuje wykonie odpowiedniej akcji lub wyświetla komunikat z prośbą o powtórzenie gestu. 

##Wykorzystane technologie##

* .Net WPF Application
* Entity Framework
* Wzorzec MVVM
* TCP Socket


##Okni logowania do aplikacji##
![ScreenShot](https://bytebucket.org/pracowniagisowa/gestapp/raw/56db16e8e4eedbbbf6287e1ed25af11195c6afcf/screeny/2017-10-31%2022_16_51-GEST%20%28Running%29%20-%20Microsoft%20Visual%20Studio.png)

##Okno nagrywania gestów##
![ScreenShot](https://bytebucket.org/pracowniagisowa/gestapp/raw/56db16e8e4eedbbbf6287e1ed25af11195c6afcf/screeny/2017-10-31%2022_26_16-GEST%20%28Running%29%20-%20Microsoft%20Visual%20Studio.png)

##Panel zarządzania##
![ScreenShot](https://bytebucket.org/pracowniagisowa/gestapp/raw/56db16e8e4eedbbbf6287e1ed25af11195c6afcf/screeny/2017-10-31%2022_38_50-GEST%20%28Running%29%20-%20Microsoft%20Visual%20Studio.png)

